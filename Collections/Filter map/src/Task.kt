// Return the set of cities the customers are from
fun Shop.getCitiesCustomersAreFrom(): Set<City> {
    val listCities = this.customers.map { it.city }
    return listCities.toSet()
}

// Return a list of the customers who live in the given city
fun Shop.getCustomersFrom(city: City): List<Customer> = this.customers.filter { it.city == city }
