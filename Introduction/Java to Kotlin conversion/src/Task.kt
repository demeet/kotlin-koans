fun toJSON(collection: Collection<Int>): String {
    val sb = StringBuilder()
    val iterator = collection.iterator()
    sb.append("[")
    while (iterator.hasNext()) {
        val element = iterator.next()
        sb.append(element)
        if (iterator.hasNext()) {
            sb.append(", ")
        }
    }
    sb.append("]")
    return sb.toString()
}
